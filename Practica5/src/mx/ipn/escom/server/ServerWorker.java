package mx.ipn.escom.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import mx.ipn.escom.utils.HTTPAnalyzer;
import mx.ipn.escom.utils.HTTPRequest;
import mx.ipn.escom.utils.HTTPResponseBuilder;

public class ServerWorker extends Thread{
	private DataInputStream ois;
	private DataOutputStream oos;
	private HTTPAnalyzer httpAnalyzer;
	private HTTPResponseBuilder responseBuilder;
	private Socket cl;
	
	public ServerWorker(Socket cl) {
		try {
			this.cl = cl;
			httpAnalyzer = new HTTPAnalyzer();
			responseBuilder = new HTTPResponseBuilder();
			oos = new DataOutputStream(cl.getOutputStream());
			ois = new DataInputStream(cl.getInputStream());			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void run() {
		byte[] request = new byte[1000];
		try {
			ois.read(request);
			HTTPRequest httpRequest = HTTPAnalyzer.analyzeRequest(request);
			if(httpRequest.getMethod().equals("GET")) {
				httpRequest.attendGet(oos);
			}
			oos.close();
			ois.close();
			cl.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
