package mx.ipn.escom.server;

import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	
	private static final String HOST = "127.0.0.1";
	private static final int PORT = 8100;
	
	public Server() {
		try {
			ServerSocket ss = new ServerSocket(PORT);
			while(true) {
				Socket cl = ss.accept();
				new ServerWorker(cl).run();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void main(String...strings ) {
		new Server();
	} 

}
