package mx.ipn.escom.client;

import java.util.Scanner;

import mx.ipn.escom.utils.HTTPConstructor;
import mx.ipn.escom.utils.HTTPResponseAnalyzer;

public class Client {
	
	private HTTPConstructor httpConstructor;
	
	public Client() {
		Scanner sc = new Scanner(System.in);
		while(true) {
			System.out.println("*************** Escribe el nombre de un archivo ****************");
			String file = sc.nextLine();
			httpConstructor = new HTTPConstructor();
			String response = httpConstructor.build(file);
			HTTPResponseAnalyzer.analyze(response);
		}
	}
	
	public static void main(String...strings) {
		new Client();
	}
}
