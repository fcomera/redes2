package mx.ipn.escom.utils;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class HTTPConstructor {
	/*
	 * Construye un request
	 * */
	
	private static final String LINE_1 = "%s %s %s\n";
	private static final String LINE_2 = "User-Agent: %s\n";
	private static final String LINE_3 = "Host: %s\n";
	private static final String LINE_4 = "Content-Type: %s\n";
	private static final String LINE_5 = "Content-Length: %s\n";
	private static final String LINE_6 = "Connection: Keep-Alive";
	
	private Socket sc;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	
	public HTTPConstructor() {
		super();
		try {
			sc = new Socket("127.0.0.1", 8100);
			oos = new ObjectOutputStream(sc.getOutputStream());
			ois = new ObjectInputStream(sc.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String build(String fileName) {
		StringBuilder httpValue = new StringBuilder();
		httpValue.append(
				String.format(
						LINE_1, "GET",  "/".concat(fileName), "HTTP/1.1"
				)
		);
		httpValue.append(
				String.format(
						LINE_2, "Java-Console"
				)
		);
		httpValue.append(
				String.format(
						LINE_3, "127.0.0.1"
				)
		);
		httpValue.append(
				String.format(LINE_4, "text/plain; charset=utf-8")
		);
		httpValue.append(
				String.format(LINE_5, "0")
		);
		httpValue.append(LINE_6);
		return sendHttp(httpValue.toString());
	}

	
	public String sendHttp(String httpValue) {
		try {
			oos.writeObject(httpValue);
			return (String)ois.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

}
