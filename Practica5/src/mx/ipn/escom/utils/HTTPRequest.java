package mx.ipn.escom.utils;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;

public class HTTPRequest {
	private static String RESOURCE_ROOT = "/Users/francisco/Documents/ESCOM/Semestre201/Redes2/P1/Practicas/Practica5/resources/%s";
	
	private String method;
	private String data;
	private String resource;
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getResource() {
		return resource;
	}
	public void setResource(String resource) {
		this.resource = resource;
	}
	
	public String attendGet(DataOutputStream oos) throws IOException {
		File f = new File(String.format(RESOURCE_ROOT, resource));
		if (f.exists()) {
			if( f.isDirectory() ) {
				return HTTPResponseBuilder.writeListContentResponse(f, oos);
			}
			return HTTPResponseBuilder.writeFile(f, oos);
		}
		
		return HTTPResponseBuilder.write404Response(oos);
	}
	
	public String attendPost() {
		return null;
	}
}
