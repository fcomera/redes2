package mx.ipn.escom.utils;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

public class HTTPResponseBuilder {
	private static final String LINE_1 = "HTTP/1.1 %s %s\n";
	private static final String LINE_2 = "Date: %s\n";
	private static final String LINE_3 = "Server: %s\n";
	private static final String LINE_4 = "Content-Type: %s\n";
	private static final String LINE_5 = "Content-Length: %s\n";
	private static final String LINE_6 = "Connection: keep-alive\n\n";
	private static final String LINE_7 = ""; // BODY
	
	private static final String NOT_FOUND_HTML_TEMPLATE = "<html><body><h1>404 Not Found</h1></body></html>";
	private static final String LIST_DIR_TEMPLATE = "<html><body><table><tr><th>Name</th><th>Size</th></tr>%s</table></body></html>";
	private static final String A_HREF_TEMPLATE = "<a href=%s>%s</a>";
	
	public static void appendDate(StringBuilder stringBuilder) {
		stringBuilder.append(
				String.format(LINE_2, ZonedDateTime.now().format(DateTimeFormatter.RFC_1123_DATE_TIME))
		);
	}
	
	public static void appendServer(StringBuilder stringBuilder) {
		stringBuilder.append(
				String.format(LINE_3, "Java-Server")
		);
	}
	
	public static void appendOKResponse(StringBuilder stringBuilder) {
		stringBuilder.append(
				String.format(LINE_1, "200", "OK")
		);
	}
	
	public static void appendConnection(StringBuilder stringBuilder) {
		stringBuilder.append(
				LINE_6
		);
	}
	
	public static String write404Response(DataOutputStream oos) throws IOException {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(
				String.format(LINE_1, "404", "Not Found")
		);
		appendDate(stringBuilder);
		appendServer(stringBuilder);
		stringBuilder.append(
				String.format(LINE_4, "text/html")
		);
		stringBuilder.append(
				String.format(LINE_5, NOT_FOUND_HTML_TEMPLATE.length())
		);
		appendConnection(stringBuilder);
		stringBuilder.append(NOT_FOUND_HTML_TEMPLATE);
		oos.write(stringBuilder.toString().getBytes());
		oos.flush();
		return stringBuilder.toString();
	}
	
	public static String writeFile(File f, DataOutputStream oos) throws IOException {
		DataInputStream dataInputStream = new DataInputStream(new FileInputStream(f));
		ByteArrayOutputStream bas = new ByteArrayOutputStream();
		ByteArrayOutputStream AUX = new ByteArrayOutputStream();
		
		StringBuilder builder = new StringBuilder();
		appendOKResponse(builder);
		appendDate(builder);
		appendServer(builder);
		Path path = f.toPath();
		URLConnection connection = f.toURL().openConnection();
	    String mimeType = connection.getContentType();
		builder.append(
				String.format(LINE_4, mimeType)
		);
		builder.append(
				String.format(LINE_5, String.valueOf(f.length()))
		);
		appendConnection(builder);
		bas.write(builder.toString().getBytes());
		Files.copy(path, AUX);
		bas.write(AUX.toByteArray());
		oos.write(bas.toByteArray());
		oos.flush();
		dataInputStream.close();
		bas.close();
		return builder.toString();
	}
	
	public static String writeListContentResponse(File f, DataOutputStream oos) throws IOException {
		StringBuilder builder = new StringBuilder();
		appendOKResponse(builder);
		appendDate(builder);
		appendServer(builder);
		builder.append(
				String.format(LINE_4, "text/html; charset=utf-8")
		);
		appendConnection(builder);
		StringBuilder aux = new StringBuilder();
		for (File archivo : f.listFiles()) {
			aux.append("<tr>");
			aux.append("<td>");
			aux.append(
					String.format(A_HREF_TEMPLATE, "\"/"+ archivo.getName() +"\"", archivo.getName())
			);
			aux.append("</td>");
			aux.append("<td>");
			aux.append(String.valueOf(archivo.length()));
			aux.append("</td>");
			aux.append("</tr>");
		}
		builder.append(
				String.format(LIST_DIR_TEMPLATE, aux.toString())
		);
		oos.write(builder.toString().getBytes());
		return builder.toString();
	}

}
