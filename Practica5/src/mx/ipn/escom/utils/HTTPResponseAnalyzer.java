package mx.ipn.escom.utils;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

public class HTTPResponseAnalyzer {
	
	public static void analyze(String response) {
		String[] separetedResponse = response.split("\n\n");
		String headers = Arrays.copyOfRange(separetedResponse, 0 , 1)[0];
		String[] headersAux = headers.split("\n");
		String body = Arrays.copyOfRange(separetedResponse, 1, 2)[0];
		printHeaders(headersAux);
		Collection<String> ss = Arrays.asList(headersAux);
		String contentType = ss.stream().filter(s -> s.contains("Content-Type")).findAny().orElse(null);
		openData(contentType, body);
	}
	
	public static void printHeaders(String...strings) {
		for(String a : strings) {
			System.out.println(a);
		}
	}
	
	public static void openData(String contentType, String body) {
		String[] content = contentType.split(":");
		if (content[1].contains("text/plain")) {
			System.out.println("\n");
			System.out.println(body);
		}
		if( content[1].contains("text/html")) {
			System.out.println(body);
		}
	}
}
