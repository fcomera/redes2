package mx.ipn.escom.utils;

public class HTTPAnalyzer {
	
	public static HTTPRequest analyzeRequest(byte[] request) {
		HTTPRequest httpRequest = new HTTPRequest();
		String requestInStringFormat = new String(request);
		httpRequest.setMethod(HTTPAnalyzer.getMethod(requestInStringFormat));
		httpRequest.setData(HTTPAnalyzer.getData(requestInStringFormat));
		httpRequest.setResource(HTTPAnalyzer.getResource(requestInStringFormat));
		return httpRequest;
	}
	
	public static String getMethod(String request) {
		String requestLine = request.split("\n")[0];
		System.out.println(requestLine);
		return requestLine.split(" ")[0];
	}
	
	public static String getResource(String request) {
		String requestLine = request.split("\n")[0];
		return requestLine.split(" ")[1];
	}
	
	public static String getData(String request) {
		return null;
	}	
}
