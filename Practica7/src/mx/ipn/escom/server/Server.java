package mx.ipn.escom.server;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import mx.ipn.escom.concrete.ConcreteCalculator;
import mx.ipn.escom.interfaces.CalculadoraInterface;

public class Server {
	public static void main(String... args) {
		try {
			CalculadoraInterface calculadoraInterface = new ConcreteCalculator();
			Registry registry = LocateRegistry.createRegistry(1099);
			registry.bind("calc", calculadoraInterface);			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
