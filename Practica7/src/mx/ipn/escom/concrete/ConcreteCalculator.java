package mx.ipn.escom.concrete;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import mx.ipn.escom.interfaces.CalculadoraInterface;

public class ConcreteCalculator extends UnicastRemoteObject implements CalculadoraInterface {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ConcreteCalculator() throws RemoteException {
		super();
	}

	@Override
	public double add(double x, double y) throws RemoteException {
		return x + y;
	}

	@Override
	public double substract(double x, double y) throws RemoteException {
		return x - y;
	}

	@Override
	public double multi(double x, double y) throws RemoteException {
		return x * y;
	}

	@Override
	public double div(double x, double y) throws RemoteException {
		if (y == 0) {
			return -1;
		}
		return x / y;
	}
}
