package mx.ipn.escom.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CalculadoraInterface extends Remote {

	public double add(double x, double y) throws RemoteException;
	public double substract(double x, double y) throws RemoteException;
	public double multi(double x, double y) throws RemoteException;
	public double div(double x, double y) throws RemoteException;
}
