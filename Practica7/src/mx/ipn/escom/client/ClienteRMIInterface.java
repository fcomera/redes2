package mx.ipn.escom.client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

public class ClienteRMIInterface extends JFrame {
	
	private String action = "";
	private String firstOperator;
	private String secondOperator;
	private ClienteRMI cliente;
	
	public ClienteRMIInterface() {
		super("Calculadora con RMI");
		cliente = new ClienteRMI();
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setSize(new Dimension(500, 500));
		setLayout(new BorderLayout());
		
		JPanel calculatorResults = new JPanel();
		calculatorResults.setLayout(new BoxLayout(calculatorResults, BoxLayout.Y_AXIS));
		
		JTextField textField = new JTextField();
		textField.setColumns(20);
		textField.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
		
		JTextField ac = new JTextField();
		ac.setColumns(20);
		ac.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
		ac.setEditable(false);
		
		calculatorResults.add(ac);
		calculatorResults.add(textField);
		
		
		JPanel basics = new JPanel();
		basics.setLayout(new BoxLayout(basics, BoxLayout.X_AXIS));
		
		JButton sumar = new JButton("sumar");
		JButton restar = new JButton("restar");
		JButton multiplicar = new JButton("multiplicar");
		JButton dividir = new JButton("dividir");
		
		JButton send = new JButton("Enviar");
		
		sumar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				action = "SUMAR";
				validateNumber(textField.getText());
				firstOperator = textField.getText();
				textField.setText("");
				ac.setText(firstOperator + " + ");
			}
		});
		
		restar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				action = "RESTAR";
				validateNumber(textField.getText());
				firstOperator = textField.getText();
				textField.setText("");
				ac.setText(firstOperator + " - ");
			}
		});
		
		multiplicar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				action = "MULTIPLICAR";
				validateNumber(textField.getText());
				firstOperator = textField.getText();
				textField.setText("");
				ac.setText(firstOperator + " * ");
			}
		});
		
		dividir.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				action = "DIVIDIR";
				validateNumber(textField.getText());
				firstOperator = textField.getText();
				textField.setText("");
				ac.setText(firstOperator + " / ");	
			}
		});
	
		
		
		
		send.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				validateNumber(textField.getText());
				secondOperator = textField.getText();
				textField.setText("");
				double respuesta = 0;
				switch (action) {
				case "SUMAR":
					try {
						respuesta = cliente.sumar(
								Double.valueOf(firstOperator),
								Double.valueOf(secondOperator)
						);
						ac.setText(String.valueOf(respuesta));
					} catch (NumberFormatException | RemoteException e1) {
						e1.printStackTrace();
					}
					break;
					
				case "RESTAR":
					try {
						respuesta = cliente.restar(
								Double.valueOf(firstOperator),
								Double.valueOf(secondOperator)		
						);
					} catch (NumberFormatException | RemoteException e1) {
						e1.printStackTrace();
					}
					ac.setText(String.valueOf(respuesta));
					break;
					
				case "MULTIPLICAR":
					try {
						respuesta = cliente.multiplicar(
								Double.valueOf(firstOperator),
								Double.valueOf(secondOperator)		
						);
					} catch (NumberFormatException | RemoteException e1) {
						e1.printStackTrace();
					}
					ac.setText(String.valueOf(respuesta));
					break;
					
				case "DIVIDIR":
					try {
						respuesta = cliente.dividir(
								Double.valueOf(firstOperator),
								Double.valueOf(secondOperator)		
						);
					} catch (NumberFormatException | RemoteException e1) {
						e1.printStackTrace();
					}
					ac.setText(String.valueOf(respuesta));
					break;
				default:
					break;
				}
			}
		});
		
		basics.add(sumar);
		basics.add(restar);
		basics.add(multiplicar);
		basics.add(dividir);
		add(calculatorResults, BorderLayout.NORTH);
		add(basics, BorderLayout.LINE_START);
		add(send, BorderLayout.SOUTH);
		pack();
		setResizable(false);
		setVisible(true);
	}
	
	public void validateNumber(String t) {
		try {
			Double.valueOf(t);
		} catch(NumberFormatException a) {
			a.printStackTrace();
		}
	}

	public static void main(String...args) {
		new ClienteRMIInterface();
	}
}
