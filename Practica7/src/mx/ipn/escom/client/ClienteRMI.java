package mx.ipn.escom.client;

import java.rmi.Naming;
import java.rmi.RemoteException;

import mx.ipn.escom.interfaces.CalculadoraInterface;

public class ClienteRMI {
	private CalculadoraInterface calculadora;
	
	public ClienteRMI() {
		try {
			calculadora = (CalculadoraInterface) Naming.lookup("rmi://127.0.0.1:1099/calc");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public double sumar(double x, double y) throws RemoteException {
		return calculadora.add(x, y);
	}
	
	public double restar(double x, double y) throws RemoteException {
		return calculadora.substract(x, y);
	}
	
	public double multiplicar(double x, double y) throws RemoteException {
		return calculadora.multi(x, y);
	}
	
	public double dividir(double x, double y) throws RemoteException {
		return calculadora.div(x, y);
	}
}
