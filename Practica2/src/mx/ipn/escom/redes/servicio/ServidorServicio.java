package mx.ipn.escom.redes.servicio;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.ImageIcon;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;

import mx.ipn.escom.redes.entitities.Articulo;
import mx.ipn.escom.redes.entitities.ArticuloCarrito;
import mx.ipn.escom.redes.entitities.ArticuloPredicates;

public class ServidorServicio {
	private ServerSocket ss;
	private static final int PUERTO = 1500;
	private ObjectInputStream lector;
	private ObjectOutputStream escritor;
	private Collection<Articulo> articulos;

	public ServidorServicio() {
		try {
			ss = new ServerSocket(PUERTO);
			setArticulos();
			while(true) {
				Socket cl = ss.accept();
				lector = new ObjectInputStream(cl.getInputStream());
				escritor = new ObjectOutputStream(cl.getOutputStream());
				for(;;){
					String command = getCommand();
					System.out.println(command);
					if (command.equals("GET_ARTICULOS")) {
						sendArticulos();
					}
					if (command.equals("GET_FOTO_ARTICULO")) {
						sendFotoArticulo();
					}
					if (command.equals("FINALIZAR_COMPRA")) {
						finalizarCompra();
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getCommand() throws IOException{
		String cmd = lector.readUTF();
		while(cmd.equals(""))
			cmd = lector.readUTF();
		return cmd;
	}
	
	public void sendArticulos() throws IOException {
		escritor.writeObject(articulos);
		escritor.flush();
	}
	
	public void sendFotoArticulo() throws IOException {
		int idArticulo = lector.readInt();
		int idFoto = lector.readInt();
		Articulo articulo = articulos
				.stream()
				.filter(ArticuloPredicates.findById(idArticulo))
				.findAny()
				.orElse(null);
		ImageIcon img = new ImageIcon(articulo.getPaths()[idFoto]);
		escritor.writeObject(img);
		escritor.flush();
	}
	
	public void finalizarCompra() throws IOException, ClassNotFoundException{
		Collection<ArticuloCarrito> carrito = (Collection<ArticuloCarrito>) lector.readObject();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		LocalDateTime ldt = LocalDateTime.now();
		PdfWriter pdfWriter = new PdfWriter("/Users/francisco/Documents/ESCOM/Semestre201/Redes2/P1/Practicas/Practica2/paths/report.pdf");
		PdfDocument pdfDocument = new PdfDocument(pdfWriter);
		Document d = new Document(pdfDocument);
		
		Paragraph title = new Paragraph("Orden de compra");
		Paragraph subtitle = new Paragraph("En este documento se encuentra la especificación de los artículos que decidiste comprar.");
		Paragraph fin = new Paragraph("Este reporte fue generado por ITEXTPDF para la orden que finalizó el dia " + dtf.format(ldt));
		
		float[] point = {100F, 100F, 100F, 100F, 100F};
		Table tabla = new Table(point);
		
		double total = 0;
		
		Cell artTitle = new Cell();
		artTitle.add(new Paragraph("Artículo"));
		Cell artDescripcion = new Cell();
		artDescripcion.add(new Paragraph("Descripción"));
		Cell artPrecio = new Cell();
		artPrecio.add(new Paragraph("Precio Unitario"));
		Cell artCantidad = new Cell();
		artCantidad.add(new Paragraph("Cantidad"));
		Cell artTotal = new Cell();
		artTotal.add(new Paragraph("Total"));
		Cell totalOrden = new Cell();
		totalOrden.add(new Paragraph("Total de la orden"));
		Cell totalOrdenNumero = new Cell();
		
		tabla.addCell(artTitle);
		tabla.addCell(artDescripcion);
		tabla.addCell(artPrecio);
		tabla.addCell(artCantidad);
		tabla.addCell(artTotal);
		
		for (ArticuloCarrito articuloCarrito: carrito) {
			Cell articulo = new Cell();
			Cell descripcion = new Cell();
			Cell precio = new Cell();
			Cell cantidad = new Cell();
			Cell totalCell = new Cell();
			double totalArticulo = 0;
			
			articulo.add(new Paragraph(articuloCarrito.getArticulo().getNombre()));
			descripcion.add(new Paragraph(articuloCarrito.getArticulo().getDescripcion()));
			precio.add(new Paragraph(String.valueOf(articuloCarrito.getArticulo().getPrecio())));
			cantidad.add(new Paragraph(String.valueOf(articuloCarrito.getCantidad())));
			totalArticulo = (articuloCarrito.getArticulo().getPrecio() * articuloCarrito.getCantidad());
			total += total + totalArticulo;
			totalCell.add(new Paragraph(String.valueOf(totalArticulo)));
			
			tabla.addCell(articulo);
			tabla.addCell(descripcion);
			tabla.addCell(precio);
			tabla.addCell(cantidad);
			tabla.addCell(totalCell);
		}
		
		totalOrdenNumero.add(new Paragraph(String.valueOf(total)));
		
		tabla.addCell(new Paragraph());
		tabla.addCell(new Paragraph());
		tabla.addCell(new Paragraph());
		tabla.addCell(totalOrden);
		tabla.addCell(totalOrdenNumero);
		
		d.add(title);
		d.add(subtitle);
		d.add(tabla);
		d.add(fin);
		d.close();
		
		File f = new File("/Users/francisco/Documents/ESCOM/Semestre201/Redes2/P1/Practicas/Practica2/paths/report.pdf");
		escritor.writeObject(f);
	}
	
	public static void main() {
		new ServidorServicio();
	}
	
	
	public void setArticulos() {
		articulos = new ArrayList<Articulo>();
		String[] paths = {
				"/Users/francisco/Documents/ESCOM/Semestre201/Redes2/P1/Practicas/Practica2/paths/img1.jpg",
				"/Users/francisco/Documents/ESCOM/Semestre201/Redes2/P1/Practicas/Practica2/paths/img2.jpg",
				"/Users/francisco/Documents/ESCOM/Semestre201/Redes2/P1/Practicas/Practica2/paths/img3.jpg",
		};
		articulos.add(
				new Articulo(
						1, "Articulo 1", "Articulo 1", 200,
						10, paths, "")
		);
		articulos.add(
				new Articulo(
						2, "Articulo 2", "Articulo 2", 300,
						10, paths, "")
		);
		articulos.add(
				new Articulo(
						3, "Articulo 3", "Articulo 3", 400,
						10, paths, "")
		);
		articulos.add(
				new Articulo(
						4, "Articulo 4", "Articulo 4", 500,
						10, paths, "")
		);
	}
	
	public static void main(String [] args) {
		new ServidorServicio();
	}
}
