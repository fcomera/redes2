package mx.ipn.escom.redes.servicio;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Collection;

import javax.swing.ImageIcon;

import mx.ipn.escom.FileUtils;
import mx.ipn.escom.redes.entitities.Articulo;
import mx.ipn.escom.redes.entitities.ArticuloCarrito;

public class ClienteServicio {
	private Socket cl;
	private ObjectOutputStream escritor;
	private ObjectInputStream lector;
	private static final int PUERTO = 1500;
	private static final String HOST = "127.0.0.1";
	private static final String FOLDER = "/Users/francisco/Documents/ESCOM/Semestre201/Redes2/P1/Practicas/Practica2/client_reports/%s";
	public ClienteServicio() {
		try {
			cl = new Socket(HOST, PUERTO);
			escritor = new ObjectOutputStream(cl.getOutputStream());
			lector = new ObjectInputStream(cl.getInputStream());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public Collection<Articulo> getArticulos() {
		try {
			escritor.writeUTF("GET_ARTICULOS");
			escritor.flush();
			return  (Collection<Articulo>) lector.readObject();
		} catch (ClassNotFoundException  | IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public ImageIcon getImg(int idArticulo, int idImg) {
		try {
			escritor.writeUTF("GET_FOTO_ARTICULO");
			escritor.flush();
			escritor.writeInt(idArticulo);
			escritor.flush();
			escritor.writeInt(idImg);
			escritor.flush();
			return (ImageIcon) lector.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	public void sendCarrito(Collection<ArticuloCarrito> articuloCarrito) {
		try {
			escritor.writeUTF("FINALIZAR_COMPRA");
			escritor.flush();
			escritor.writeObject(articuloCarrito);
			escritor.flush();
			File f = (File) lector.readObject();
			FileUtils.copyFromFile(String.format(FOLDER, f.getName()), f);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
