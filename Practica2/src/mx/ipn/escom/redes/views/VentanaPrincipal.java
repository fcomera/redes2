package mx.ipn.escom.redes.views;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

import mx.ipn.escom.redes.entitities.Articulo;
import mx.ipn.escom.redes.entitities.ArticuloCarrito;
import mx.ipn.escom.redes.servicio.ClienteServicio;

public class VentanaPrincipal extends JFrame {
	private JButton verCarrito;
	private JButton finalizarCompra;
	private ClienteServicio clienteServicio;
	private Collection<ArticuloCarrito> carrito;
	private Collection<Articulo> articulosFromService; 
	
	public VentanaPrincipal() {
		clienteServicio = new ClienteServicio();
		articulosFromService = clienteServicio.getArticulos();
		carrito = new ArrayList<>();
		
		setLayout(new BorderLayout());
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		verCarrito = new JButton("Ver carrito");
		finalizarCompra = new JButton("Finalizar compra");
		
		JPanel articulos = new JPanel();
		articulos.setLayout(new BoxLayout(articulos, BoxLayout.Y_AXIS));
		for(Articulo articulo : articulosFromService) {
			articulos.add(new ArticuloView(articulo, carrito, clienteServicio));
		}
		
		JPanel botones = new JPanel();
		botones.setLayout(new BoxLayout(botones, BoxLayout.X_AXIS));
		botones.add(verCarrito);
		botones.add(finalizarCompra);
		
		verCarrito.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new CarritoView(carrito);
			}
		});
		
		finalizarCompra.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				clienteServicio.sendCarrito(carrito);
			}
		});
		
		add(new JScrollPane(articulos), BorderLayout.CENTER);
		add(botones, BorderLayout.PAGE_END);
		pack();
		setVisible(true);
	}
	public static void main(String[] args) {
		new VentanaPrincipal();
	}
	
	public void setCarrito() {
		carrito = new ArrayList<>();
		for(Articulo articulo : articulosFromService) {
			new ArticuloCarrito(articulo, 0);
		}
	}
}
