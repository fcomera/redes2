package mx.ipn.escom.redes.views;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import mx.ipn.escom.redes.entitities.Articulo;
import mx.ipn.escom.redes.entitities.ArticuloCarrito;

public class CarritoView extends JFrame{
	private JButton comprar;
	private JButton regresar;
	
	
	public CarritoView(Collection<ArticuloCarrito> carrito) {
		setLayout(new BorderLayout());
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		
		comprar = new JButton("Comprar");
		regresar = new JButton("Regresar");
		
		JPanel arts = new JPanel();
		arts.setLayout(new BoxLayout(arts, BoxLayout.Y_AXIS));
		for (ArticuloCarrito a : carrito) {
			arts.add(new ArticuloCarritoView(a, carrito));
		}
		
		JPanel botones = new JPanel();
		botones.setLayout(new BoxLayout(botones, BoxLayout.X_AXIS));
		botones.add(regresar);
		
		regresar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		add(arts, BorderLayout.CENTER);
		add(botones, BorderLayout.PAGE_END);
		pack();
		setVisible(true);
	}

}
