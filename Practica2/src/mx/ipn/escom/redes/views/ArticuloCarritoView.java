package mx.ipn.escom.redes.views;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import mx.ipn.escom.redes.entitities.ArticuloCarrito;

public class ArticuloCarritoView extends JPanel {
	public ArticuloCarritoView(ArticuloCarrito articuloCarrito, Collection<ArticuloCarrito> carrito) {
		super();
		setLayout(new BorderLayout());
		JPanel info = new JPanel();
		info.setLayout(new BoxLayout(info, BoxLayout.Y_AXIS));
		
		info.add(new JLabel(articuloCarrito.getArticulo().getNombre()));
		info.add(new JLabel(articuloCarrito.getArticulo().getDescripcion()));
		info.add(new JLabel(articuloCarrito.getArticulo().getPromocion()));
		info.add(new JLabel(String.valueOf(articuloCarrito.getArticulo().getPrecio())));
		
		JPanel docs = new JPanel();
		docs.add(new JLabel("x"));
		
		
		JPanel actions = new JPanel();
		JSpinner artCantidad = new JSpinner();
		artCantidad.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if ((int)artCantidad.getValue()  < 1 ){
					artCantidad.setValue(1);
					articuloCarrito.setCantidad(1);
				}
				if ((int) artCantidad.getValue() > articuloCarrito.getArticulo().getExistencias()) {
					artCantidad.setValue(articuloCarrito.getArticulo().getExistencias());
					articuloCarrito.setCantidad(articuloCarrito.getArticulo().getExistencias());
				}
				articuloCarrito.getArticulo().setExistencias(articuloCarrito.getArticulo().getExistencias() - 1);
				articuloCarrito.setCantidad(articuloCarrito.getCantidad() + 1);
			}
		});
		
		JButton quitar = new JButton("Quitar");
		quitar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				carrito.remove(articuloCarrito);
				getParent().getParent().remove(getParent());
				getParent().getParent().revalidate();
				getParent().getParent().repaint();
			}
		});
		artCantidad.getModel().setValue(articuloCarrito.getCantidad());
		actions.setLayout(new BorderLayout());
		actions.add(quitar, BorderLayout.PAGE_START);
		actions.add(artCantidad, BorderLayout.CENTER);

		
		add(info, BorderLayout.LINE_START);
		add(docs, BorderLayout.CENTER);
		add(actions, BorderLayout.LINE_END);
	}
}
