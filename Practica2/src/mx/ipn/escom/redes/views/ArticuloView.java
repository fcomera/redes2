package mx.ipn.escom.redes.views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Random;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import mx.ipn.escom.redes.entitities.Articulo;
import mx.ipn.escom.redes.entitities.ArticuloCarrito;
import mx.ipn.escom.redes.entitities.ArticuloPredicates;
import mx.ipn.escom.redes.servicio.ClienteServicio;

public class ArticuloView extends JPanel{
	private JButton back;
	private JButton next;
	private ImageIcon imagenes;
	private JButton add;
	private JLabel titulo;
	private JLabel img;
	private JLabel precio;
	private JLabel disponibilidad;
	
	public ArticuloView(Articulo articulo, Collection<ArticuloCarrito> carrito, ClienteServicio clienteServicio) {
		super();
		setLayout(new BorderLayout(10,10));
		
		imagenes = new ImageIcon();
		img = new JLabel(imagenes);
		img.setIcon(clienteServicio.getImg(articulo.getId(), 0));
		img.setMaximumSize(new Dimension(100, 100));
		
		back = new JButton("<");
		back.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Random random = new Random();
				int i = random.nextInt(3);
				img.setIcon(clienteServicio.getImg(articulo.getId(), i));
				img.setMaximumSize(new Dimension(100, 100));
			}
		});
		
		next = new JButton(">");
		next.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Random random = new Random();
				int i = random.nextInt(3);
				img.setIcon(clienteServicio.getImg(articulo.getId(), i));
				img.setMaximumSize(new Dimension(100, 100));
			}
		});
		
		
		titulo = new JLabel(articulo.getDescripcion());
		precio = new JLabel(String.valueOf(articulo.getPrecio()));
		disponibilidad = new JLabel(String.valueOf(articulo.getExistencias()));
		
		add = new JButton("Agregar");
		add.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				addToCarrito(articulo, carrito);
				disponibilidad.setText(String.valueOf(articulo.getExistencias()));
			}
		});
		JButton eliminar = new JButton("Eliminar");
		eliminar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				carrito.remove(articulo);
			}
		});
		

		JPanel detail = new JPanel();
		detail.setLayout(new BoxLayout(detail, BoxLayout.Y_AXIS));
		detail.add(titulo);
		detail.add(precio);
		detail.add(disponibilidad);
		detail.add(add);
		detail.add(eliminar);
		
		JPanel imgs = new JPanel();
		imgs.setLayout(new BorderLayout(10, 10));
		
		imgs.add(back, BorderLayout.LINE_START);
		imgs.add(img, BorderLayout.CENTER);
		imgs.add(next, BorderLayout.LINE_END);
		
		
		add(imgs, BorderLayout.CENTER);
		add(detail, BorderLayout.LINE_END);
		
	}
	
	public void addToCarrito(Articulo a, Collection<ArticuloCarrito> carrito) {
		if(articuloInCarrito(a, carrito)) {
			ArticuloCarrito articuloCarrito = ArticuloCarrito.findByArticuloId(a, carrito);
			articuloCarrito.setCantidad(articuloCarrito.getCantidad() + 1);
		}else {
			carrito.add(new ArticuloCarrito(a, 1));
		}
		a.setExistencias(a.getExistencias() - 1);
	}
	
	public boolean articuloInCarrito(Articulo a, Collection<ArticuloCarrito> carrito) {
		return ArticuloCarrito.findByArticuloId(a, carrito) != null;
	}
}
