package mx.ipn.escom.redes.entitities;

import java.io.Serializable;
import java.util.Collection;

public class ArticuloCarrito implements Serializable{
	private Articulo articulo;
	private int cantidad;
	public ArticuloCarrito(Articulo articulo, int cantidad) {
		super();
		this.articulo = articulo;
		this.cantidad = cantidad;
	}
	public Articulo getArticulo() {
		return articulo;
	}
	public void setArticulo(Articulo articulo) {
		this.articulo = articulo;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	public static ArticuloCarrito findByArticuloId(Articulo a, Collection<ArticuloCarrito> carrito) {
		return carrito
				.stream()
				.filter(articulo -> articulo.getArticulo().getId() == a.getId())
				.findAny()
				.orElse(null);
	}
}
