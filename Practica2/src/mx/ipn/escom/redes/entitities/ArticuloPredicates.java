package mx.ipn.escom.redes.entitities;

import java.util.function.Predicate;

public class ArticuloPredicates {
	
	public static Predicate<Articulo> findById(Integer id) {
		return p -> p.getId() == id;
	}

}
