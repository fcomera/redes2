package mx.ipn.escom;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileUtils {

	public static File copyFromFile(String newFile, File originFile) {
		File archivo = new File(newFile);
		try(
				InputStream is = new FileInputStream(originFile); 
				OutputStream os = new FileOutputStream(archivo)
			) {
			byte[] buffer = new byte[1024];
			while(is.read(buffer) > 0) {
				os.write(buffer);
			}
			return archivo;
		} catch(IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}
