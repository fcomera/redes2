package escom.ipn.mx;

import java.awt.*;
import java.net.*;
import java.io.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedInputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.TimeUnit;
import java.nio.charset.Charset;
import java.nio.ByteBuffer;

import javax.swing.*;

public class Tablero implements ActionListener{
	/*Zona de variables*/
	private static String test;
	private JFrame tablero= new JFrame("Busca Minas");
	private static JTextArea principal;
	private JButton aceptar; 
	private String dificultad;
	private int filas;
	private int columnas;
	private static boolean valido=false;
	private static String coordenada;
	
	public Tablero(String diff) {
		/*Setea la dificultad*/
		setDificultad(diff);
		setRango(Integer.parseInt(getDificultad()));
		
		tablero.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		tablero.setSize(680,550);
		tablero.setLocationRelativeTo(null);
		
		/*Menu Bar*/
		JMenuBar mb = new JMenuBar();       
        JMenu m1 = new JMenu("Juego");       
        JMenu m2 = new JMenu("Informacion");       
        mb.add(m1);       
        mb.add(m2);       
        JMenuItem m11 = new JMenuItem("Nuevo");       
        JMenuItem m22 = new JMenuItem("Salir");
        JMenuItem m33 = new JMenuItem("Ayuda");
        m11.addActionListener(this);
        m22.addActionListener(this);
        m33.addActionListener(this);
        m1.add(m11);       
        m1.add(m22); 
        m2.add(m33);
        
        // Creando el panel en la parte inferior y agregando componentes       
        JPanel panel = new JPanel(); // el panel no está visible en la salida
        JLabel msg = new JLabel("/*--Introduce una cordenada--*/");
        
        JPanel center= new JPanel();
        JLabel cordx = new JLabel("Numero de fila:");
        JTextField tx = new JTextField(2);
        JLabel cordy = new JLabel("Numero de columna:");  
        JTextField ty = new JTextField(2);
        
        center.add(cordy);
        center.add(ty);
        center.add(cordx);
        center.add(tx);
        
        aceptar = new JButton("Aceptar"); 
        aceptar.addActionListener(new ActionListener() {
        	
        	public void actionPerformed(ActionEvent e) {
        	
        		if(tx.getText().equals("") || ty.getText().equals("")) {
        			JOptionPane.showMessageDialog(null,"Alguno de los campos es vacio.","Error de campos",JOptionPane.ERROR_MESSAGE);
        		}else {
        			System.out.println(tx.getText()+","+ty.getText());
            		valido=cordValida(Integer.parseInt(tx.getText()),Integer.parseInt(ty.getText()));
            		if(valido==true) {
            			System.out.println("Correcto");
            			setCoordenada(tx.getText()+","+ty.getText());
            			
            		}else {
            			JOptionPane.showMessageDialog(null,"La coordena introducida no es valida."
            					+"\nCoordenada introducida: "+ty.getText()+"x"+tx.getText()
            					+ "\nEl tamaño del tablero es de "+getFilas()+"x"+getColumnas(),"Coordenada invalida",JOptionPane.INFORMATION_MESSAGE);
            		}
        		}
        	}
        });
              
        panel.add(BorderLayout.NORTH,msg);      
        panel.add(BorderLayout.CENTER,center);
        panel.add(BorderLayout.SOUTH,aceptar);
        
        /*Label con el tablero*/       
        principal.setEditable(false);
        
        // Agregar componentes al marco.      
        tablero.getContentPane().add(BorderLayout.NORTH, mb); 
        tablero.getContentPane().add(BorderLayout.CENTER,principal);
        tablero.getContentPane().add(BorderLayout.SOUTH, panel);        
		
	}
	/*Metodo Main*/
	public static void main(String[] args) {
		String[] dificultad= {"1","2","3"};
		Tablero tablero;
		String dif =(String) JOptionPane.showInputDialog(null,"Selecciona una dificultad\n1-Facil.\n2.-Medio\n3.-Dificil","Bienvedio a Busca Minas",JOptionPane.INFORMATION_MESSAGE
				,null,dificultad,dificultad[0]);
		if(dif!=null) {
			switch(dif) {
				case "1":
					dif="0";
					tablero = new Tablero(dif);
					tablero.mostrar();
					break;
				case "2":
					dif="1";
					tablero = new Tablero(dif);
					tablero.mostrar();
					break;
					
				case "3":
					dif="2";
					tablero = new Tablero(dif);
					tablero.mostrar();
					break;
			}
			
			try {
				int port=9999;
	            String host="127.0.0.1";	            
	            Socket socket= new Socket(host,port);
	            System.out.println("Conexion con servidor exitosa");
	            PrintWriter pw = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
	            BufferedInputStream br2=new BufferedInputStream(socket.getInputStream());
                pw.print(Integer.parseInt(dif));
	            pw.flush();
                
                byte[] b = new byte[1000]; 
                br2.read(b, 0, 1000);
                String campo =  new String(b);
                principal.setText(campo);
                System.out.println(campo);

	            while(true){
		            TimeUnit.SECONDS.sleep(1);
                    if(valido) {
                        pw.print(getCoordenada());
		                pw.flush();
                        byte[] zz = new byte[1000];
                        br2.read(zz, 0, 1000);
		                String eco = new String(zz);
                        System.out.println("Eco: "+eco);
                        principal.setText(eco);
                        if(eco.equals("Fin juego")) {
                    	    System.out.println("Termina aplicacion");
                            br2.close();                       
                            pw.close();
                            socket.close();
                        }
                        valido = false;
                    }
	            }
			} catch (Exception e) {
				 e.printStackTrace();
			}
			

		}
		
		System.out.println(dif);
	}
	
	void mostrar(){
		tablero.setVisible(true);
	}
	
	boolean cordValida(int x,int y) {
		if(x>=0 && x<= getColumnas()) {
			if(y>=0 && y<= getFilas()) {
				return true;
			}
		}
		return false;
	}
	
	void setRango(int diff){
		System.out.println(diff);
		String aux;
		switch(diff) {
		case 0:
			setFilas(8);
			setColumnas(8);
			this.principal= new JTextArea(getTest());
			break;
		case 1:
			setFilas(16);
			setColumnas(16);
			this.principal= new JTextArea(getTest());
			break;
		case 2:
			setFilas(16);
			setColumnas(30);
			this.principal= new JTextArea(getTest());
			break;
		}
	}
	
	
	public String getDificultad() {
		return dificultad;
	}

	public void setDificultad(String dificultad) {
		this.dificultad = dificultad;
	}

	public int getFilas() {
		return filas;
	}

	public void setFilas(int filas) {
		this.filas = filas;
	}

	public int getColumnas() {
		return columnas;
	}

	public void setColumnas(int columnas) {
		this.columnas = columnas;
	}
	
	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}
	
	public static String getCoordenada() {
		return coordenada;
	}
	public static void setCoordenada(String coordenada) {
		Tablero.coordenada = coordenada;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String choice = e.getActionCommand();
			switch(choice) {
			case "Salir":
				JOptionPane.showMessageDialog(null,"Hasta la proxima :)","Salida",JOptionPane.INFORMATION_MESSAGE);
		        System.exit(0);
				break;
			case "Nuevo":
				JOptionPane.showMessageDialog(null,"Presiono nuevo","Nuevo juego",JOptionPane.INFORMATION_MESSAGE);
				break;
			case "Ayuda":
				JOptionPane.showMessageDialog(null,"Se debe introducir el numero de fi y de columna y luego presionar el boton aceptar para seleccionar una casilla","Ayuda",JOptionPane.INFORMATION_MESSAGE);
				break;
			}
		
	}
	
}
