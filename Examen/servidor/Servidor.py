import socketserver
from Buscaminas import Buscaminas
from Buscaminas import DIFICULTAD_PRINCIPIANTE, DIFICULTAD_INTERMEDIA, DIFICULTAD_AVANZADA
from Buscaminas import MinaEncontradaException


class Servidor(socketserver.BaseRequestHandler):

    def escribir(self, data):
        data += '\n'
        print(data)
        return self.request.sendall(bytes(data, 'utf-8'))

    def leer(self):
        return self.request.recv(1024).strip()

    def handle(self):
        print(self.client_address[0] + ':' + str(self.client_address[1]))

        reader: str = ''
        
        reader = self.leer()
        dificultad = int((str(reader, 'utf-8')))
        
        self.buscaminas = Buscaminas(dificultad)
        self.escribir(self.buscaminas.get_campo())
        
        try:
            while(True):
                reader: str = ''
                reader = str(self.leer(), 'utf-8')
                lista = reader.split(',')
                coordenada = tuple([int(lista[0]), int(lista[1])])
                self.buscaminas.sel_lugar(coordenada)
                self.escribir(self.buscaminas.get_campo())
        except MinaEncontradaException:
            self.escribir('Fin juego')

if __name__ == '__main__':
    HOST = 'localhost'
    PORT = 9999
    with socketserver.TCPServer((HOST, PORT), Servidor) as servidor:
        print('SERVIDOR INICIADO')
        servidor.serve_forever()
