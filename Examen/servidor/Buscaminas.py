import numpy as np
import random

COORDENADA_X = 0
COORDENADA_Y = 1

MINA = -1
MINA_EXPLOTADA = 0

NORTE = 0
SUR = 1
ESTE = 2
OESTE = 3

DIFICULTAD_PRINCIPIANTE = 0
DIFICULTAD_INTERMEDIA = 1
DIFICULTAD_AVANZADA = 2

class MinaEncontradaException(Exception):
    pass

class Elemento(object):
    def __init__(self,  coordenada):
        self.coordenada = coordenada
        self.vecinos = None

    def set_vecinos(self, vecinos):
        self.vecinos = vecinos
 
    def has_north(self):
        if self.vecinos[NORTE] is None:
            return False
        if isinstance(self.vecinos[NORTE], Mina):
            return False
        return True

    def has_south(self):
        if self.vecinos[SUR] is None:
            return False
        if isinstance(self.vecinos[NORTE], Mina):
            return False
        return True

    def has_east(self):
        if self.vecinos[ESTE] is None:
            return False 
        if isinstance(self.vecinos[NORTE], Mina):
            return False
        return True

    def has_west(self):
        if self.vecinos[OESTE] is None:
            return False
        if isinstance(self.vecinos[NORTE], Mina):
            return False
        return True

    def __str__(self):
        return '(' + ','.join([str(self.coordenada[COORDENADA_X]), str(self.coordenada[COORDENADA_Y])]) + ')'

class Mina(Elemento):
    def __init__(self, coordenada):
        self.valor = MINA
        super().__init__(coordenada)

    def explotar(self):
        self.valor = MINA_EXPLOTADA


class LugarSeguro(Elemento):
    def __init__(self, coordenada):
        self.valor = '*'
        super().__init__(coordenada)

    def has_mina_in_north(self):
        if self.has_north():
            if isinstance(self.vecinos[NORTE], Mina):
                return True
        return False

    def has_mina_in_south(self):
        if self.has_south():
            if isinstance(self.vecinos[SUR], Mina):
                return True
        return False

    def has_mina_in_east(self):
        if self.has_east():
            if isinstance(self.vecinos[ESTE], Mina):
                return True
        return False

    def has_mina_in_west(self):
        if self.has_west():
            if isinstance(self.vecinos[OESTE], Mina):
                return True
        return False

    def rec(self):
        valor_final = 0
        if self.has_mina_in_north():
            valor_final = valor_final + 1
        if self.has_mina_in_south():
            valor_final = valor_final + 1
        if self.has_mina_in_east():
            valor_final = valor_final + 1
        if self.has_mina_in_west():
            valor_final = valor_final + 1
        if valor_final == 0:
            self.valor = ' '
        else:
            self.valor = valor_final
        return self.valor

class Campo(object):

    def __init__(self, dificultad=0):
        self.rows = 0
        self.cols = 0
        self.dificultad = dificultad
        self.set_size()
        self.set_minas_num()
        self.set_elementos()

    def set_size(self):
        self.rows = self.get_rows()
        self.cols = self.get_cols()
 
    def get_cols(self):
        if self.cols == 0:
            if self.dificultad == DIFICULTAD_PRINCIPIANTE:
                self.cols = 9
            if self.dificultad == DIFICULTAD_INTERMEDIA:
                self.cols = 16
            if self.dificultad == DIFICULTAD_AVANZADA:
                self.cols = 30
        return self.cols

    def get_rows(self): 
        if self.rows == 0 or self.rows is None:
            if self.dificultad == DIFICULTAD_PRINCIPIANTE:
                self.rows = 9
            else:
                self.rows = 16
        return self.rows

    def set_minas_num(self): 
        if self.dificultad == DIFICULTAD_PRINCIPIANTE:
           self.minas = 10
        if self.dificultad == DIFICULTAD_INTERMEDIA:
           self.minas = 40
        if self.dificultad == DIFICULTAD_AVANZADA:
           self.minas = 99

    def set_elementos(self):
        self.minas_ananidas = 0
        i = 0
        j = 0
        self.elementos = np.array(
            [
                [
                    self.set_elemento(y, x) for x in range(self.cols)
                ]
                for y in range(self.rows)
            ]
        )
        self.set_vecinos()

    def set_elemento(self, i, j): 
        coordenada = tuple([i, j])
        if random.randint(0, self.minas) % self.minas < 1:
            return Mina(coordenada)
        return LugarSeguro(coordenada)
    
    def set_vecinos(self):
        i = 0
        for fila in self.elementos:
            j = 0
            for el in fila:
                vecinos = [None, None, None, None]
                if i == 0:
                    vecinos[NORTE] = None
                    if j == 0:
                        vecinos[OESTE] = None
                    else:
                        vecinos[OESTE] = self.elementos.item((i, j - 1))
                    if j == self.rows - 1:
                        vecinos[ESTE] = None
                    else:
                        vecinos[ESTE] = self.elementos.item((i, j + 1))
                    vecinos[SUR] = self.elementos.item((i + 1, j))
                elif i == self.rows - 1: 
                    vecinos[SUR] = None
                    vecinos[NORTE] = self.elementos.item((i - 1, j))
                    if j == 0:
                        vecinos[OESTE] = None
                    else:
                        vecinos[OESTE] = self.elementos.item((i, j - 1))
                    if j == self.cols - 1:
                        vecinos[ESTE] = None
                    else:
                        vecinos[ESTE] = self.elementos.item((i, j + 1))
                else:
                    vecinos[NORTE] = self.elementos.item((i - 1, j))
                    vecinos[SUR] = self.elementos.item((i + 1, j))
                    if j == 0:
                        vecinos[OESTE] = None
                    else:
                        vecinos[OESTE] = self.elementos.item((i, j - 1))
                    if j == self.rows - 1:
                        vecinos[ESTE] = None
                    else:
                        vecinos[ESTE] = self.elementos.item((i, j + 1))
                el.set_vecinos(vecinos)
                j += 1
            i += 1

    def print_campo(self):
        for fila in self.elementos:
            cadena = ''
            for el in fila:
                cadena += str(el)
                cadena += ' '
            print(cadena)

    def print_campo_minado(self):
        for fila in self.elementos:
            cadena = ''
            for el in fila:
                cadena += str(el.valor)
                cadena += '\t'
            print(cadena)

    def print_campo_oculto(self): 
        for fila in self.elementos:
            cadena = ''
            for el in fila:
                if isinstance(el, Mina):
                    cadena += '*'
                else:
                    cadena += str(el.valor)
                cadena += '\t'
            print(cadena)

    def sel_lugar(self, coordenada):
        elemento = self.elementos.item((coordenada[COORDENADA_X], coordenada[COORDENADA_Y]))
        if isinstance(elemento, Mina):
            raise MinaEncontradaException
        else:
            self.recorrido(elemento)

    def recorrido(self, elemento):
        if isinstance(elemento, Mina):
            return
        if elemento.has_north():
            self.recorrido(elemento.vecinos[NORTE])
        if elemento.has_east():
            self.recorrido(elemento.vecinos[ESTE])
        #if elemento.has_south():
        #    self.recorrido(elemento.vecinos[SUR])
        #if elemento.has_west():
        #    self.recorrido(elemento.vecinos[OESTE])
        return elemento.rec()

    def get_campo(self):
        campo = ''
        for fila in self.elementos:
            for el in fila:
                if isinstance(el, Mina):
                    campo += '*'
                else:
                    campo += str(el.valor)
                campo += ' '
            campo += '\n'
        return campo

class Buscaminas(object):

    def __init__(self, dificultad=0):
        self.campo = Campo(dificultad)

    def sel_lugar(self, coordenada):
        self.campo.sel_lugar(coordenada)

    def print_campo(self):
        self.campo.print_campo_oculto()

    def get_campo(self):
        return self.campo.get_campo()
