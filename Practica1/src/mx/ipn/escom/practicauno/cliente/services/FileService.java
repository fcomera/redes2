package mx.ipn.escom.practicauno.cliente.services;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import mx.ipn.escom.practicauno.constants.Constants;
import mx.ipn.escom.practicauno.servidor.FileUtils;

public class FileService {
	private Socket clSocket;
	private ObjectInputStream reader;
	private ObjectOutputStream writer;
	public static final String UNIX_ROOT = "/home/francisco/Documents";
	public static final String OSX_ROOT = "/Users/francisco/Documents/ESCOM/Semestre201/Redes2/P1/Practicas/Practica1/LOCAL/";
	public static final String TEST = "/Users/francisco/Documents/ESCOM/Semestre201/Redes2/P1/Practicas/Practica1/%s";
	public static final String TEST_DOS = "/Users/francisco/Documents/ESCOM/Semestre201/Redes2/P1/Practicas/Practica1/LOCAL/%s";
	
	public FileService(){
		try {
			clSocket = new Socket(Constants.HOST, Constants.PORT);
			reader = new ObjectInputStream(clSocket.getInputStream());
			writer = new ObjectOutputStream(clSocket.getOutputStream());
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Error al conectarse", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
	}
	
	public static File[] getLocalFiles() {
		String os = System.getProperty("os.name").toLowerCase();
		File dir;
		if(os.indexOf("mac") >= 0 )
			dir = new File(OSX_ROOT);
		else
			dir = new File(UNIX_ROOT);
		return dir.listFiles();
	}
	
	public static String getRoot() {
		return "LOCAL";
	}
	
	public static DefaultMutableTreeNode getFilesInTree(File f) {
		String[] spl = f.getAbsolutePath().split("/");
		if(f.isDirectory()) {
			DefaultMutableTreeNode auxroot = new DefaultMutableTreeNode(spl[spl.length-1]);
			for(File aux: f.listFiles()) {
				auxroot.add(FileService.getFilesInTree(aux));
			}
			return auxroot;
		}
		return new DefaultMutableTreeNode(spl[spl.length-1]);
	}
	
	public DefaultMutableTreeNode getRemoteFiles() {
		try {
			return (DefaultMutableTreeNode)reader.readObject();	
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Error al recibir archivos remotos", "Error", JOptionPane.ERROR_MESSAGE);
			return null;
		}
	}
	
	public void sendLocalFiles(TreePath[] tree) {
		try {
			List<String> paths = transformTreePathToPaths(tree);
			for(String path : paths) {
				writer.writeUTF("ENVIAR");
				writer.flush();
				sendFile(new File(path));
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void sendFile(File f) {
		/*
		 * Enviar un archivo
		 * */
		try {
			writer.writeObject(f);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<String> transformTreePathToPaths(TreePath[] tree) {
		List<String> paths = new ArrayList<>();
		for(TreePath p: tree) {
			Object[] path = p.getPath();
			List<String> aux = new ArrayList<>();
			for(Object s: path) {
				aux.add(s.toString());
			}
			paths.add(String.join("/", aux));
		}
		return paths;
	}
	
	
	public void getRemoteFile(TreePath[] tree) {
		try {
			List<String> paths = transformTreePathToPaths(tree);
			for(String path : paths) {
				writer.writeUTF("DESCARGAR");
				writer.flush();
				writer.writeUTF(path);
				writer.flush();
				File f = (File) reader.readObject();
				if (f.isDirectory()) {
					writeDirectory(f);
				}else {
					writeFile(f);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void writeFile(File f, String...strings) {
		if (strings.length > 0 ) {
			String n = f.getName();
			String nombre = String.format(TEST_DOS, String.join("/", strings[0], n));
			FileUtils.copyFromFile(nombre, f);
		}else {
			FileUtils.copyFromFile(
				String.format(TEST_DOS, f.getName()),
				f);
		}
	}
	
	public void writeDirectory(File f, String...strings) {
		if (f.isDirectory()) {
			File dir = null;
			if(strings.length > 0) {
				String nombre = f.getName();
				dir = new File(String.format(TEST_DOS, String.join("/", strings[0], nombre)));
			}else {
				dir = new File(String.format(TEST_DOS, f.getName()));
			}
			dir.mkdirs();
			for (File archivo : f.listFiles()) {
				if (archivo.isDirectory()) {
					writeDirectory(archivo, getParentPath(archivo));
				}else {
					writeFile(archivo, getParentPath(archivo));
				}
			}
		}
	}
	
	public String getParentPath(File archivo) {
		String[] aux = archivo.getParent().split("/");
		String[] c = Arrays.copyOfRange(aux, 1, aux.length);
		List<String> d = Arrays.asList(c);
		return String.join("/", d);
	}
	
	public void close() throws IOException{
		writer.writeUTF("CLOSING");
		reader.close();
		writer.close();
		clSocket.close();
	}
}
