package mx.ipn.escom.practicauno.cliente.views;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.WindowConstants;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import mx.ipn.escom.practicauno.cliente.services.FileService;

public class VentanaView extends JFrame implements WindowListener{
	private JTree local;
	private JTree remoto;
	private JPanel panelUno;
	private JPanel panelDos;
	private FileService fileService;
	private JButton accion;
	private JButton download;
	
	private String[] data = {"one", "two", "three"};
	
	public static void main(String[] args) {
		new VentanaView("Practica");
	}
	
	public VentanaView(String title) {
		super(title);
		fileService = new FileService();
		setLists();
		EnviarActionListener eal = new EnviarActionListener();
		accion = new JButton("Enviar");
		download = new JButton("Descargar");
		
		
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setSize(new Dimension(500,500));
		setLayout(new BorderLayout());
		
		panelUno = new JPanel(new BorderLayout());
		JScrollPane a = new JScrollPane(local);
		a.setPreferredSize(new Dimension(250, 250));
		panelUno.add(a, BorderLayout.PAGE_START);
		panelUno.add(accion, BorderLayout.PAGE_END);
		
		
		panelDos = new JPanel(new BorderLayout());
		JScrollPane b = new JScrollPane(remoto);
		b.setPreferredSize(new Dimension(250, 250));
		panelDos.add(b, BorderLayout.PAGE_START);
		panelDos.add(download, BorderLayout.PAGE_END);
		
		add(panelUno, BorderLayout.WEST);
		add(panelDos, BorderLayout.EAST);
		JButton reset = new JButton("RESET");
		reset.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DefaultTreeModel model = (DefaultTreeModel) local.getModel();
				DefaultMutableTreeNode node = new DefaultMutableTreeNode(FileService.getRoot());
				for(File f: FileService.getLocalFiles()) {
					node.add(FileService.getFilesInTree(f));
				}
				model.setRoot(node);
				model.reload();
			}
		});
		add(reset, BorderLayout.SOUTH);
		setVisible(true);
		setResizable(false);
		pack();
		accion.setActionCommand("ENVIAR");
		download.setActionCommand("DESCARGAR");
		accion.addActionListener(eal);
		download.addActionListener(eal);
	}
	
	public void setLists() {
		remoto = new JTree(fileService.getRemoteFiles());  // Datos del servidor
		DefaultMutableTreeNode root = new DefaultMutableTreeNode(FileService.getRoot());
		for(File f: FileService.getLocalFiles()) {
			root.add(FileService.getFilesInTree(f));
		}
		local = new JTree(root);
	}
	
	public class EnviarActionListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if(e.getActionCommand().equals("ENVIAR")){
				fileService.sendLocalFiles(local.getSelectionPaths());
				DefaultTreeModel model = (DefaultTreeModel) remoto.getModel();
				model.setRoot(fileService.getRemoteFiles());
				model.reload();
			}
			if(e.getActionCommand().equals("DESCARGAR")) {
				fileService.getRemoteFile(remoto.getSelectionPaths());
				DefaultTreeModel model = (DefaultTreeModel) local.getModel();
				DefaultMutableTreeNode node = new DefaultMutableTreeNode(FileService.getRoot());
				for(File f: FileService.getLocalFiles()) {
					node.add(FileService.getFilesInTree(f));
				}
				model.setRoot(node);
				model.reload();
			}
		}
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		try {
			fileService.close();
		} catch(Exception em) {
			em.printStackTrace();
		}
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
}
