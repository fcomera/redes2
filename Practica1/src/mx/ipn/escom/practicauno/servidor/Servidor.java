package mx.ipn.escom.practicauno.servidor;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.List;

import javax.swing.tree.DefaultMutableTreeNode;

import mx.ipn.escom.practicauno.cliente.services.FileService;
import mx.ipn.escom.practicauno.constants.Constants;

public class Servidor {
	private ServerSocket ss;
	private ObjectOutputStream writer;
	private ObjectInputStream reader;
	public static final String OSX_ROOT = "/Users/francisco/Documents/ESCOM/Semestre201/Redes2/P1/Practicas/Practica1/REMOTO/";
	public static final String TEST = "/Users/francisco/Documents/ESCOM/Semestre201/Redes2/P1/Practicas/Practica1/REMOTO/%s";
	public static final String TEST_DOS = "/Users/francisco/Documents/ESCOM/Semestre201/Redes2/P1/Practicas/Practica1/";
	
	public Servidor() {
		try {
			ss = new ServerSocket(Constants.PORT);
			System.out.println("---> Servicio listo");
			while(true) {
				Socket cl = ss.accept();
				writer = new ObjectOutputStream(cl.getOutputStream());
				reader = new ObjectInputStream(cl.getInputStream());
				sendLocalFiles(cl);
				for(;;) {
					String command = getInstruction(cl);
					if (command.equals("ENVIAR")) {
						getFiles(cl);	
					}
					if (command.equals("DESCARGAR")) {
						sendFiles(cl);
					} 
					if (command.equals("CLOSING")) {
						writer.close();
						reader.close();
						cl.close();
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getInstruction(Socket cl) {
		String g;
		try {
			g = reader.readUTF();
			while(g.equals("")) 
				g = reader.readUTF();
			System.out.println(g);
			 return g;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private void sendFiles(Socket cl) {
		try {
			String fileName = replacePath(reader.readUTF());
			File file = new File(fileName);
			writer.writeObject(file);
			writer.flush();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}

	private String replacePath(String path) {
		return path;
	}

	private void getFiles(Socket cl) {
		try {
			File f = (File) reader.readObject();
			if (f.isDirectory()) {
				writeDirectory(f);
			}
			else {
				writeFile(f);
			}
			sendLocalFiles(cl);
		} catch(ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeFile(File f, String... strings) {
		/*
		 * Guarda un archivo en el sistema remoto de archivos
		 * */
		if (strings.length > 0) {
			String n = f.getName();
			String nombre = String.format(TEST, String.join("/", strings[0], n));
			FileUtils.copyFromFile(nombre, f);
		} else {
			FileUtils.copyFromFile(String.format(TEST, f.getName()), f);
		}
	}
	
	public void writeDirectory(File f, String...strings) {
		if (f.isDirectory()) {
			File dir = null;
			if(strings.length > 0) {
				String nombre = f.getName();
				dir = new File(String.format(TEST, String.join("/", strings[0], nombre)));
			}else {
				dir = new File(String.format(TEST, f.getName()));
			}
			dir.mkdirs();
			for (File archivo : f.listFiles()) {
				if (archivo.isDirectory()) {
					writeDirectory(archivo, getParentPath(archivo));
				}else {
					writeFile(archivo, getParentPath(archivo));
				}
			}
		}
	}
	
	public String getParentPath(File archivo) {
		String[] aux = archivo.getParent().split("/");
		String[] c = Arrays.copyOfRange(aux, 1, aux.length);
		List<String> d = Arrays.asList(c);
		return String.join("/", d);
	}

	public void sendLocalFiles(Socket cl) {
		try {
			DefaultMutableTreeNode root = new DefaultMutableTreeNode("REMOTO");
			File f = new File(OSX_ROOT);
			for(File arc : f.listFiles()) {
				root.add(FileService.getFilesInTree(arc));
			}
			writer.writeObject(root);
			writer.flush();
			System.out.println("Se envio el directorio remoto");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public DefaultMutableTreeNode getFile (File f) {
		String [] spl = f.getPath().split("/");
		if (f.isDirectory()) {
			DefaultMutableTreeNode nuevo = new DefaultMutableTreeNode(spl[spl.length - 1] + "/");
			for (File aux: f.listFiles()) {
				nuevo.add(getFile(aux));
			}
		}
		return new DefaultMutableTreeNode(spl[spl.length - 1]);
	}
	
	public static void main(String[] args) {
		new Servidor();
	}
}
